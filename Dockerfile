FROM ubuntu:latest
MAINTAINER Douglas Ianitsky "ianitsky@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

RUN dpkg-reconfigure debconf

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update
RUN apt-get install -y software-properties-common wget git curl composer php-pear
RUN add-apt-repository ppa:ondrej/php
RUN apt-get update
RUN apt-get install -y graphviz
RUN apt-get install -y openjdk-11-jre
RUN apt-get install -y libxext-dev
RUN apt-get install -y libxrender-dev
RUN apt-get install -y libxtst-dev
RUN apt-get install -y libgtk2.0-bin
RUN apt-get install -y libsecret-1-0
RUN apt-get install -y gnome-keyring

RUN apt-get install -y php-xdebug
RUN apt-get install -y php-xml
RUN apt-get install -y php5.6-cli php5.6-xml
RUN apt-get install -y php7.0-cli php7.0-xml
RUN apt-get install -y php7.1-cli php7.1-xml
RUN apt-get install -y php7.2-cli php7.2-xml
RUN apt-get install -y php7.3-cli php7.3-xml

RUN apt-get autoremove
RUN apt-get clean

RUN wget -c http://static.phpmd.org/php/latest/phpmd.phar -O /usr/bin/phpmd.phar && chmod +x /usr/bin/phpmd.phar
RUN pear install PHP_CodeSniffer

RUN rm -rf /var/lib/apt/lists/*
RUN rm -rf /tmp/*

RUN useradd -m -s /bin/bash developer \
    && mkdir /home/developer/.PhpStorm2018.2 \
    && touch /home/developer/.PhpStorm2018.2/.keep \
    && chown -R developer.developer /home/developer \
    && mkdir /opt/phpstorm \
    && wget -O - https://download-cf.jetbrains.com/webide/PhpStorm-2018.2.4.tar.gz | tar xzf - --strip-components=1 -C "/opt/phpstorm"

USER developer
VOLUME /home/developer/.PhpStorm2018.2
CMD /opt/phpstorm/bin/phpstorm.sh
